terraform {
  backend "s3" {
    bucket  = "tfstate-us-east-2"
    key     = "terraform.tfstate"
    region  = "us-east-2"
    encrypt = true
  }
}

provider "aws" {
  version = "~> 1.46.0"
  region  = "us-east-2"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "LambdaRunRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "LogsAndDynamoDbForLambda"
  role = "${aws_iam_role.iam_for_lambda.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:DescribeStream",
                "dynamodb:ListTables",
                "dynamodb:GetRecords",
                "dynamodb:GetShardIterator",
                "dynamodb:ListStreams",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_lambda_function" "my_lambda" {
  function_name    = "LambdaTesting"
  handler          = "lambda"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  runtime          = "go1.x"
  description      = "Testing lambda calls"
  memory_size      = 128
  filename         = "../lambda.zip"
  source_code_hash = "${base64sha256(file("../lambda.zip"))}"
  timeout          = 20
}
