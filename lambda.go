package main

import (
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func handler() (string, error) {
	sess, err := session.NewSession()
	if err != nil {
		return "", err
	}

	ddb := dynamodb.New(sess)
	res, err := ddb.ListTables(&dynamodb.ListTablesInput{})
	if err != nil {
		return "", err
	}
	return res.String(), nil
}

func main() {
	lambda.Start(handler)
}
