aws-lambda-dynamodb
===================

This code shows how to create a simple AWS lambda which interacts with
DynamoDB and how to deploy it using Terraform to AWS.

The lambda code is written in Go.


Requirements
------------
* AWS account set up and ready to use
* Go 1.11.x (at least i tested it with 1.11.2)
* Terraform

Build & Deploy
--------------

```
$ export AWS_PROFILE=default # eventually adjust this to profile you use
$ make init                  # initialize, only needed once
$ make                       # this compiles Go code and packages the lambda
$ make deploy                # terraform apply the stack

```
