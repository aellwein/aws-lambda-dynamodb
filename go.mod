module CEPBEPOK/aws-deploy

require (
	github.com/aws/aws-lambda-go v1.8.0
	github.com/aws/aws-sdk-go v1.16.11
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/net v0.0.0-20181220203305-927f97764cc3 // indirect
	golang.org/x/text v0.3.0 // indirect
)
