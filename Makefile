PROFILE = ${AWS_PROFILE}

all:	lambda lambda.zip

init:
	env GO111MODULE=auto go get -u github.com/aws/aws-lambda-go/cmd/build-lambda-zip
	cd tf && env AWS_PROFILE=$(PROFILE) terraform init && cd ..

lambda:	lambda.go
	go build -o $@ $<

lambda.zip: lambda
	build-lambda-zip -o $@ $<


deploy:	all
	cd tf && env AWS_PROFILE=$(PROFILE) terraform apply -auto-approve

destroy:
	cd tf && env AWS_PROFILE=$(PROFILE) terraform destroy -auto-approve

clean:
	$(RM) lambda lambda.zip

.PHONY:	clean
